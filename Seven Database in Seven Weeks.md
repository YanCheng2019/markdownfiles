# Seven Databases in Seven  Weeks


## Chapter 1  Introduction

What  database or combination of databases best resolves your problem?
## Heading
- Database type.
Databases come in a variety of genres,  such as relational (Postgres, MariaDB), key-value (Redis), columnar (HBase, Cassandra), document-oriented (MongoDB, CouchDB), and graph (Neo4J), and maybe cloud-based database that’s a difficult-to-classify hybrid (DynamoDB).
- Driving force.
RDBMS databases arose in a world where query flexibility was more important than flexible schemas. On the other hand, column-oriented databases were built to be well suited for storing large amounts of data across several machines, while data relationships took a backseat.
- Interface.
A database has an interactive command-line interface, programming interface. At a lower level, we’ll discuss protocols such as REST (CouchDB) and Thrift (HBase).
- Unique features.
 Can the database query on arbitrary fields? Index for rapid lookup? Support ad hoc queries or queries must be planned for others？Has a rigid framework or merely a set of guidelines?
- Performance.
How does this database function and at what cost?  Does it support sharding, replication? Does it distribute data evenly using consistent hashing, or does it keep like data together? Is this database tuned for reading, writing, or some other operation? How  much control do you have over its tuning, if any?
- Scalability.
Scalability is related to performance. While the discussion on how to scale each database will be intentionally light, each database is geared more for horizontal scaling  (MongoDB, HBase, DynamoDB), traditional vertical scaling (Postgres,  Neo4J, Redis), or something in between.

### The Genres

**Relational**

PostgreSQL​, MySQL, H2, HSQLDB, SQLite, and many others.

- Relational database management systems (RDBMSs)  
are set-theory-based systems implemented as two-dimensional tables with rows and strictly enforced column types.
- The canonical means of interacting with an RDBMS is to write queries in Structured Query Language (SQL).
- Data values are typed and may be numeric, strings, dates, uninterpreted blobs, or other types. The types are enforced by the system.
- Importantly, tables can join and morph into new, more complex tables because of their mathematical basis in relational (set) theory.

**Key-Value**

Redis, DynamoDB,  Riak, memcached, Voldemort.

- The key-value (KV) store is the simplest model, and a KV store pairs keys to values in much the same way that a map (or hashtable) would in any popular programming language.
- Some KV implementations permit complex value types such as hashes or lists, or a means of iterating through the keys, but all this is an added bonus.
- The KV moniker demands so little,  databases of this type can be incredibly performant in a number of scenarios but generally won’t be helpful when you have complex query and  aggregation needs.

**Columnar**

​HBase​ and Cassandra.

- Columnar, or column-oriented, databases are designed that data from a given column (in the two dimensional table sense) is stored together. While a row-oriented database (like an RDBMS) keeps information about a row together. Each row can have a different set of columns, or none at all, allowing tables to remain sparse without incurring a storage cost for null values.
- In column-oriented databases, adding columns is quite inexpensive and is done on a row-by-row basis.
- With respect to structure, columnar is about  midway between relational and key-value.

**Document**

MongoDB​ and CouchDB

- Document-oriented databases store documents which a document is like a hash, with a unique ID field and values that may be any of a variety of types, including more hashes. 
- Documents can contain nested structures, and so they exhibit a high degree of flexibility, allowing for variable domains.
- Different document databases take different approaches with respect to indexing, ad hoc querying, replication, consistency, and other design decisions.

**Graph**

Neo4J​.

- Graph databases excel at dealing with highly interconnected data.
- One operation where other databases often fall flat is crawling through selfreferential or otherwise intricately linked data. The benefit of using a graph database is the ability to quickly traverse nodes and relationships to find relevant data.

# Chapter 2 PostgreSQL

Relational Datase
- Vast toolkits (triggers, stored procedures, views, advanced indexes).  
- Data safety (via ACID compliance)
- Mind share (many programmers speak and think relationally).
- Query pliancy.
- Don’t need to know in advance how you plan to use the data. If a relational schema is normalized, queries are flexible.

PostgreSQL

PostgreSQL is by far the oldest and most battle-tested database.

- domain-specific plug-ins:
	- natural language parsing
	- multidimensional indexing
	- geographic queries
	- custom datatypes
- sophisticated transaction handling and built-in stored procedures  
for a dozen languages
- built-in Unicode support, sequences, table inheritance, and subselects


### Day 1: Relations, CRUD, and Joins

RDBMSs are built atop a set theory  
branch called relational algebra—a combination of selections (WHERE ...),  
projections (SELECT ...), Cartesian products (JOIN ...), and more.

Relational queries are derived from a branch of mathematics known as tuple  
relational calculus, which can be converted to relational algebra. PostgreSQL and other RDBMSs optimize queries by performing this conversion and simplifying the algebra.

PostgreSQL, being of the relational style, is a design-first database. First you  
design the schema; then you enter data that conforms to the definition of that  
schema.

CRUD is a useful mnemonic for remembering the basic data management operations: Create, Read, Update, and Delete.

```sql
CREATE​ ​TABLE(
  country_code char(2) PRIMARY KEY,
  country_name text UNIQUE
​);

​CREATE​ ​TABLE​ cities (  
​  ​name​ ​text​ ​NOT​ ​NULL​,  
​  postal_code ​varchar​(9) ​CHECK(postal_code <>),
​  country_code ​char​(2) ​REFERENCES​ countries,
​ ​ PRIMARY​ ​KEY​ (country_code, postal_code)  
​ );

​CREATE​ ​TABLE​ venues (  
​ venue_id ​SERIAL​ ​PRIMARY​ ​KEY​,  
​ ​name​ ​varchar​(255),  
​ street_address ​text​,  
​ ​type​ ​char​(7) ​CHECK​ ( ​type​ ​in​ (​'public'​,​'private'​) ) ​DEFAULT​  
​'public'​,  
​ postal_code ​varchar​(9),  
​ country_code ​char​(2),  
​ ​FOREIGN​ ​KEY​ (country_code, postal_code) ​REFERENCES​ cities (country_code, postal_code) ​  
​ );
```
- The `PRIMARY KEY` nominate a unique identifier column, constrains the  column to disallow duplicate.
- the `REFERENCES` keyword constrains fields to another table’s primary key. This is  called maintaining referential integrity, and it ensures our data is always  correct.
- the `SERIAL` keyword is automatically  
incremented integer (1, 2, 3, 4, and so on). MySQL has a similar construct called AUTO_INCREMENT.

Join Reads

Inner join specify two columns (one from each table) to match by, using the ON keyword, and returns a single table.

Outer  joins are a way of merging two tables when the results of one table must  always be returned, whether or not any matching column values exist on the  
other table.

### Indexing

The speed of PostgreSQL (and any other RDBMS) lies in its efficient management of blocks of data, reduced disk reads, query optimization, and  other techniques. But those only go so far in fetching results quickly. Without an index, in a selection, each row must be  
read from disk to know whether a query should return it.

An index is a special data structure built to avoid a full table scan when performing a query. PostgreSQL automatically creates an index on the primary key—in particular a B-tree index—where the key is the primary key value and where the value points to a row on disk. Using the `UNIQUE` keyword is another way to force an index on a table column.

> The main difference between a binary tree and a B-Tree is that the latter allows for more than one data point (table row) per node. B-Trees are also balanced, which means that the time it takes to execute a search within this structure is mostly independent of the value to be found.

>B+tree is another data structure that used to store data, which looks almost the same as the B-tree. The only difference of B+tree is that it stores data on the leaf nodes. In specialty in B+tree is, you can do the same search as B-tree, and additionally, you can travel through all the values in leaf node.

Assume the following three-column table needs to be stored on a database:

| Name | Mark | Age|
|---|---|---|
|Jone | 5 | 28|
|Alex | 32 | 45|
|Tom | 37 | 23|
|Ron | 87 | 13|
|Mark | 20 | 48|
|Bob | 89 | 32|

First, the database creates a primary key for each of the given records and converts the relevant rows into a byte stream. Then, it stores each of the keys and record byte streams on a B+tree. Here, the primary key used as the key for indexing. The key and record byte stream is altogether known as `Payload`. The resulting B+tree could be represented as follows.

![B+ Tree](https://cdn-images-1.medium.com/max/1600/1*LF6-glMyvdIiAk-JykIIXw.jpeg)

Here you can see that all records are stored in the leaf nodes of the B+tree and index used as the key to creating a B+tree. No records are stored on non-leaf nodes. Each of the leaf nodes has reference to the next record in the tree. A database can perform a binary search by using the index or sequential search by searching through every element by only traveling through the leaf nodes.

When indexing is enabled, the database creates three B-trees for each of the columns in the table as follows. Here the key is the B-tree key used to indexing. The index is the reference to the actual data record.

![B-Tree](https://cdn-images-1.medium.com/max/1600/1*pXI0w1RagRCixnVnDysQvg.jpeg)

When indexing is used first, the database searches a given key in correspondence to B-tree and gets the index in O(log(n)) time. Then, it performs another search in B+tree by using the already found index in O(log(n)) time and gets the record.

Each of these nodes in B-tree and B+tree is a page in SQL Server terms. Pages are fixed in size. Pages have a unique number starting from one. A page can be a reference to another page by using page number. At the beginning of the page, page meta details such as the rightmost child page number, first free cell offset, and first cell offset stored. There can be two types of pages in a database:

1.  Pages for indexing: These pages store only index and a reference to another page.
2.  Pages to store records: These pages store the actual data and page should be a leaf page.

B-tree provides an efficient way to insert and read data. In actual Database implementation, the database uses both B-tree and B+tree together to store data. B-tree used for indexing and B+tree used to store the actual records. B+tree provides sequential search capabilities in addition to the binary search, which gives the database more control to search non-index values in a database.

You can explicitly add a hash index using the `CREATE INDEX` command,  where each value must be unique (like a hashtable or a map).

```sql
​CREATE​ ​INDEX​ events_title​ ​ON​ ​events​ ​USING​ ​hash (title);
```

It’s worth noting that when you set a FOREIGN KEY constraint, PostgreSQL will not automatically create an index on the targeted column(s). You’ll need to create an index on the targeted column(s) yourself.

### Day 2: Advanced Queries, Code, and Rules

#### Aggregate Functions

count(), max(), min()

```sql
​SELECT​ count(title) FROM​ ​events​ WHERE​ title ​LIKE​ ​'%Day%'​;

​SELECT​ min(​starts​), max(​ends​) FROM​ ​events​ ​INNER​ ​JOIN​ venues ON​ ​events ​WHERE​ venues.​name​ = ​'Crystal Ballroom'​;
```

#### Grouping

`GROUP BY` place the rows into groups and then perform some aggregate function (such as count) on those groups.

```sql
SELECT​ venue_id, count(*) ​FROM​ ​events​ ​GROUP​ ​BY​ venue_id;

  venue_id | count  
​-----------+-------​--
​         1 |      1
​ ​        2 |      2
​ ​        3 |      1
​ ​        4 |      3

​SELECT​ venue_id FROM​ ​events​ ​GROUP​ ​BY​ venue_id HAVING​ count(*) >= 2;

  venue_id | count  
​-----------+-------​--
​ ​        2 |      2

​SELECT​ ​DISTINCT​ venue_id ​FROM​ ​events​;
```

##### Window Functions

Window functions are similar to `GROUP BY` queries in that they allow you to run aggregate functions across multiple rows. The difference is that they allow you to use built-in aggregate functions without requiring every single field to be grouped to a single row. Whereas a `GROUP BY` clause will return one record per matching group value, a window function, which does not collapse the results per group, can return a separate record for each row. `PARTITION BY` returns the results of an aggregate function OVER a PARTITION of the result set.

```sql
SELECT venue_id, count(*) OVER (PARTITION BY venue_id) FROM events ORDER BY venue_id;

  venue_id | count  
​-----------+-------​--
​         1 |      1
​ ​        2 |      2
​ ​        2 |      2
​ ​        3 |      1
​ ​          |      3
​ ​          |      3
​ ​          |      3

SELECT venue_id, count(*) OVER FROM events GROUP BY venue_id ORDER BY venue_id;

  venue_id | count  
​-----------+-------​--
​         1 |      1
​ ​        2 |      2
​ ​        3 |      1
​ ​          |      3
```

#### Transactions

Transactions are the bulwark of relational database consistency. Transactions ensure that every  
command of a set is executed. If anything fails along the way, all of the  
commands are rolled back as if they never happened.

PostgreSQL transactions follow ACID compliance, which stands for:  
- Atomic (either all operations succeed or none do)  
- Consistent (the data will always be in a good state and never in an inconsistent state)  
- Isolated (transactions don’t interfere with one another)  
- Durable (a committed transaction is safe, even after a server crash)

Consistency in ACID is different from consistency in CAP Theorem​.

We can wrap any transaction within a `BEGIN TRANSACTION` block. To verify  
atomicity, we’ll kill the transaction with the `ROLLBACK` command. The events all remain.

```sql
BEGIN​ ​TRANSACTION​;
  DELETE​ ​FROM​ ​events​;  
​ROLLBACK​;  
​SELECT​ * ​FROM​ ​events​;
```
Transactions are useful when you’re modifying two tables that you don’t want out of sync.

#### Stored Procedures

Stored procedures are extremely powerful and can be used to do an enormous range of tasks, from performing complex mathematical  
operations that aren’t supported in SQL to triggering cascading series of  
events to pre-validating data before it‘s written to tables and far beyond.

Here‘s what the procedure will accomplish: if the venue doesn’t exist, it will be created first and then referenced in the new event. The procedure will also return a Boolean indicating whether a new venue was added as a helpful bonus.

```sql
-- postgres/add_event.sql

CREATE​ ​OR​ ​REPLACE​ ​FUNCTION​ add_event (  
 ​ title ​text​,  
​  ​starts​ ​timestamp​,  
​  ​ends​ ​timestamp​,  
​  venue ​text​,  
​  postal ​varchar​(9),  
​  country ​char​(2))  
​RETURNS​ ​boolean​ ​AS​ ​$$

​DECLARE​  
​ did_inser t b​oolean​ := ​false​;  
​ found_cou nt i​nteger​;  
​ the_venue _id​integer​;  

BEGIN​  
​ ​SELECT​ venue_id I​NTO​ the_venue _id FROM​ venues v ​WHERE ILIKE venue ​LIMIT​ 1;
​
  IF​ the_venue_id​  
    ​INSERT​ ​INTO​ venues (​name​, postal_code, country_code) VALUES 
    RETURNING venue_id INTO the_venue_id;
    did_insert := true;
  ENDIF;

  RAISE NOTICE ​'Venue found %'​, the_venue_id;
  
  ​INSERT​ ​INTO​ ​events​ (title, ​starts​, ​ends​, venue_id) VALUES​ (title, ​starts​, ​ends)
​  RETURN​ did_insert;
END;
​$$​ ​LANGUAGE​ plpgsql;
```
You can import this external file into the current schema using the following  
command-line argument:
```shell
dbname =# ​\​i add_event.​sql
```

This stored procedure is run as a `SELECT` statement:

```sql
​SELECT​ add_event(
  ​'House Party'​,
  ​'2018-05-03 23:00'​,
​ ​ '2018-05-04 02:00'​,
  'Run​''​s House'​, 
  ​'97206'​, ​
  'us'​);
```

In addition to PL/pgSQL (the language we used in the procedure), Postgres supports three more core languages for writing procedures: Tcl (PL/Tcl), Perl (PL/Perl), and Python (PL/Python). People have written extensions for a dozen more, including Ruby, Java, PHP Scheme, and others listed in the public documentation.

```sql
createlang​ ​dbname​ ​--list
```

Try this shell to list the languages installed in your database. The createlang command is also used to add new languages.

#### Pull the Triggers

Triggers automatically fire stored procedures when some event happens, such as an insert or update.

First, create a logs table to store event changes. A primary key isn’t necessary here because it’s just a log.

```sql
​CREATE​ ​TABLE​ ​logs​ (  
 ​ event_id ​integer​,  
​  old_title varchar​(255),  
​  old_start timestamp​,  
​  old_ends ​timestamp​,  
​  logged_at timestamp​ ​DEFAULT current_timestamp​  
​);
```

Next, we build a function to insert old data into the log. The `OLD` variable represents the row about to be changed, and `NEW` represents an incoming row. Output a notice to the console with the event_id before returning.

```sql
-- postgres/log_event.sql
CREATE​ ​OR​ ​REPLACE​ ​FUNCTION log_event() R​ETURNS​ ​trigger​ ​AS​ ​$$​  
​DECLARE​  
​BEGIN​  
​ ​INSERT​ ​INTO​ ​logs  
​ ​VALUES​ (OLD.event_id, OLD)
​ RAISE NOTICE ​'Someone just changed event #%'​, OLD.event_id;  
​ ​RETURN​ ​NEW​;  
​END​;  
​$$​ ​LANGUAGE​ plpgsql;
```

Finally, we create our trigger to log changes after any row is updated.

```sql
​CREATE​ ​TRIGGER​ log_events  
​ ​AFTER​ ​UPDATE​ ​ON​ ​events​  
​ ​FOR​ ​EACH​ ​ROW​ ​EXECUTE​ ​PROCEDURE​ log_event();
```

#### Viewing

Unlike stored procedures, views aren’t functions being executed but rather aliased queries. Views are powerful tools for opening up complex queried data in a simple way.

Let’s say that we wanted to see only holidays that contain the word Day and have no venue. We could create a `VIEW` for that like this:

```sql
-- postgres/holiday_view_1.sql
​CREATE​ ​VIEW​ holidays ​AS​
​  ​SELECT​ event_id ​AS​ holiday_i
​ ​ FROM​ ​events​
​ ​ WHERE​ title L​IKE​ ​'%Day%'​ ​AND venue_id ​IS​ ​NULL​;
```

If you want to add a new column to the holidays view, it will have to come from the underlying table. After altering the events table, we cannot use `UPDATE`​ to update a `VIEW` directly.

`View` mentioned previously are a convenient abstraction, they don’t yield any performance gains over the `SELECT` queries that they alias. If you want `VIEW`s that do offer such gains, you should consider creating materialized views, which are different because they’re stored on disk in a “real” table and thus yield performance gains because they restrict the number of tables that must be accessed to exactly one.

You can create materialized views just like ordinary views, except with a `CREATE MATERIALIZED VIEW` rather than `CREATE VIEW` statement.

The downside of materialized views is that they do increase disk space usage. But in many cases, the performance gains are worth it. In general, the more complex the query and the more tables it spans, the more performance gains you’re likely to get vis-à-vis plain old `SELECT` queries or `VIEW`s. The downside of materialized views is that they do increase disk space usage.



#### RULE

In fact, a `VIEW` is a `RULE`. A `RULE` is a description of how to alter the parsed query tree. Every time Postgres runs an SQL statement, it parses the statement into an abstract query tree.

Operators and values become branches and leaves in the tree, and the tree is  
walked, pruned, and in other ways edited before execution. This tree is optionally rewritten by Postgres rules, before being sent on to the query planner which also rewrites the tree to run optimally, and sends this final command to be executed.

We need to craft a `RULE` that tells Postgres what to do with an `UPDATE`. Our rule will capture updates to the holidays view and instead run the update on events, pulling values from the pseudorelations `NEW` and `OLD`. `NEW` functionally acts as the relation containing the values we’re setting, while `OLD` contains the values we query by.

```
-- postgres/create_rule.sql
CREATE​ RULE update_holidays
​  ​UPDATE​ ​events​
​  ​SET​ title = N​EW​.​name​,
​ ​   starts​ = ​NEW​.​date​,
​    colors = ​NEW​.colors
​ ​ WHERE​ title = OLD.na​me​;
```

With this rule in place, now we can update holidays directly. 
```sql
​UPDATE​ holidaysS​ET​ colors = '​{"red","green"}'​ ​where​ ​name​ =  
​'Christmas Day'​;
```

Fuzzy Searching
- SQL Standard String Matches
  - I Like LIKE and ILIKE
  - Regex
- Bride of Levenshtein
- Trigram

Full-Text
- TSVector and TSQuery
- Other Languages
- Indexing Lexemes
- Metaphones

Combining String Matches
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEyNTcyOTkyOTksMTQ5MjUzMjEzNCwxNj
A4NDI0NDA1LC0xMTQ0NDc1NzMsLTE1OTM5NDQzMDYsNTM1OTMw
MzU5LDMxNjIxODg0MiwxMDIxMzk2MjQsMTEwNjc1NzQyOCwxND
cwNTM1NTc2LC0xMzQxNjU2NzE3LDExMzI3NzcxMzQsLTc1OTU2
MDUxNCwxMDA4MDQ0NjgxLDEwNzIzNTc2NjcsLTIwNTEyNjYwOC
wxMzIzNDUwNzY1LDE4ODU2NjgxMSwtMTYxMzY1Nzg2MiwtODc5
MjgxMDUxXX0=
-->