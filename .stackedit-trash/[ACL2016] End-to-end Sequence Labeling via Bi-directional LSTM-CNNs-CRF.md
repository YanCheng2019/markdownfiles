# [ACL2016] End-to-end Sequence Labeling via Bi-directional LSTM-CNNs-CRF

**Date**: 2016/03
**Comments**: ACL2016
**Citations**: 707 [2019/8]


## 1 Introduction

**Motivation**

end-to-end sequence labeling, like POS, NER

**Previous works**

**Previous problems**

- hand-crafted features
- even systems that have utilized distributed representations as inputs have used these
to augment, rather than replace, hand-crafted features

**Datasets**

- Penn Treebank WSJ corpus for part-of-speech (POS)
- CoNLL 2003 corpus for named entity recognition (NER)

**Contents**:

- proposing a novel neural network architecture for linguistic sequence labeling. 
- giving empirical evaluations of this model on benchmark data sets for two classic NLP tasks. 
- achieving state-of-the-art performance with this truly end-to-end system.

## 2 Neural Network Architecture

### 2.1 CNN for Character-level Representation

- Previous studies ([Santos and Zadrozny, 2014]((http://proceedings.mlr.press/v32/santos14.pdf)); [Chiu and Nichols, 2015]((https://www.mitpressjournals.org/doi/pdf/10.1162/tacl_a_00104))) have shown that CNN is an effective approach to extract morphological information (like the prefix or suffix of a word) from characters of words and encode it into neural representations.
- A dropout layer ([Srivastava et al., 2014]((http://www.jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf?utm_content=buffer79b43&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer))) is applied before character embeddings are input to CNN.
  >[Srivastava, N., Hinton, G., Krizhevsky, A., Sutskever, I., & Salakhutdinov, R. (2014). Dropout: a simple way to prevent neural networks from overfitting. The journal of machine learning research, 15(1), 1929-1958.](http://www.jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf?utm_content=buffer79b43&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)

Thus, the character-level representation architecture:


### 2.2 BLSTM

- Basically, a LSTM unit is composed of three multiplicative gates which control the proportions of information to forget and to pass on to the next time step.
- For many sequence labeling tasks it is beneficial to have access to both past (left) and future (right) contexts. An elegant solution whose effectiveness has been proven by previous work (Dyer et al., 2015) is bi-directional LSTM (BLSTM). The basic idea is to present each sequence forwards and backwards to two separate hidden states to capture past and future information, respectively. Then the two hidden states are concatenated to form the final output.

### 2.3 CRF

- For sequence labeling (or general structured prediction) tasks, it is beneficial to consider the correlations between labels in neighborhoods and jointly decode the best chain of labels for a given input sentence. Therefore, we model label sequence jointly using a conditional random field (CRF) (Lafferty et al., 2001), instead of decoding each label independently.

- For CRF training, we use the maximum conditional likelihood estimation. For a sequence CRF model (only interactions between two successive labels are considered), training and decoding can be solved efficiently by adopting the Viterbi algorithm.

### 2.4 BLSTM-CNNs-CRF

- Step1: for each word, the character-level representation is computed by the CNN with character embeddings as inputs
- Step2: character representation vector is concatenated with the word embedding and feed into the BLSTM network, dropout layers are applied on both the input and output vectors of BLSTM.
- Step3: the output vectors of BLSTM are fed to the CRF layer to jointly decode the best label sequence.

Architecture:



## 3 Network Training


### 3.1 Parameter Initialization

**Word Embeddings**

4 type word embeddings:

- Stanford's publicly available GloVe 100-dimensional embeddings trained on 6 billion words from Wikipedia and web text (Pennington et al., 2014)
  >[Pennington, J., Socher, R., & Manning, C. (2014, October). Glove: Global vectors for word representation. In Proceedings of the 2014 conference on empirical methods in natural language processing (EMNLP) (pp. 1532-1543).](https://www.aclweb.org/anthology/D14-1162)
- Senna 50-dimensional embeddings trained on Wikipedia and Reuters RCV-1 corpus (Collobert et al., 2011),
- Google's Word2Vec 300-dimensional embeddings3 trained on 100 billion words from Google News (Mikolov et al., 2013)
- randomly initialized embeddings with 100 dimensions, where embeddings are uniformly sampled from range $\left[ -\sqrt{\frac{3}{dim}}, + \sqrt{\frac{3}{dim}} \right]$ where dim is the dimension of embeddings (Heet al., 2015)

experiments result:

- models using pretrained word embeddings obtain a significant improvement as opposed to the ones using random embeddings
- NER relies more heavily on pretrained embeddings than POS tagging. This is consistent with results reported by previous work (Collobert et al., 2011; Huang et al., 2015; Chiu and Nichols, 2015).
- Google's Word2Vec is not as good as the other two embeddings on NER is because of vocabulary mismatch. Word2Vec embeddings were trained in casesensitive manner, excluding many common symbols such as punctuations and digits.

**Character Embeddings**

- Character embeddings are initialized with uniform samples from $\left[ -\sqrt{\frac{3}{dim}}, + \sqrt{\frac{3}{dim}} \right]$ where dim is 30.

**Weight Matrices and Bias Vectors**

- Matrix parameters are randomly initialized with uniform samples from $\left[ -\sqrt{\frac{6}{r+c}}, + \sqrt{\frac{6}{r+c}} \right]$, where r and c are the number of of rows and columns in the structure (Glorot and Bengio, 2010).
- Bias vectors are initialized to zero, except the bias bf for the forget gate in LSTM , which is initialized to 1.0 (Jozefowicz et al., 2015).


### 3.2 Optimization Algorithm

**Algorithm**

- SGD, Best in experiments.
  batch size: 10, momentum: 0.9, initial learning rate: $η_0 = 0.01$ for POS tagging, and $η_0 = 0.015$ for NER. The learning rate is updated on each epoch of training as $η_t = η_0/(1 + ρ_t)$, with decay rate $ρ = 0.05$ and $t$ is the number of epoch completed. gradient clipping is 5.0, to reduce the effects of “gradient exploding” (Pascanu et al., 2012).
- AdaDelta: (Zeiler, 2012)
- Adam: (Kingma and Ba, 2014)
- RMSProp: (Dauphin et al., 2015)

**Early Stopping**

We use early stopping (Giles, 2001; Graves et al., 2013) based on performance on validation sets.

**Fine Tuning**

For each of the embeddings, we fine-tune initial embeddings, modifying them during gradient updates of the neural network model by back-propagating gradients. The effectiveness of this method has been previously explored in sequential and structured prediction problems (Collobert et al., 2011; Peng and Dredze, 2015).

**Dropout Training**

To mitigate overfitting, we apply the dropout method (Srivastava et al., 2014) to regularize our model. We fix dropout rate at 0.5 for all dropout layers through all the experiments. We obtain significant improvements on model performance after using dropout.

### 3.3 Tuning Hyper-Parameters

Tune the hyper-parameters on the development sets by random search. Due to time constrains it is infeasible to do a random search across the full hyper-parameter space. Thus, for the tasks of POS tagging and NER we try to share as many hyper-parameters as possible. Note that the final hyper-parameters for these two tasks are almost the same, except the initial learning rate. 

- We set the state size of LSTM to 200. Tuning this parameter did not significantly impact
the performance of our model.
- For CNN, we use 30 filters with window length 3.

## 4 Experiments

### 4.1 Data Sets

- POS Tagging
Wall Street Journal (WSJ) portion of Penn Treebank (PTB) (Marcus et al., 1993), 45 different POS tags, standard splits(section 0-18 as training, section 19-21 as development, section 22-24 as test) (Manning, 2011; Søgaard, 2011).

- NER
English data from CoNLL 2003 shared task (Tjong Kim Sang and De Meulder, 2003), 4 different named entities(PERSON, LOCATION, ORGANIZATION, and MISC). We use the BIOES tagging scheme instead of standard BIO2, as previous studies have reported meaningful improvement with this scheme (Ratinov and Roth, 2009; Dai et al., 2015; Lample et al., 2016).

### 4.2 dissect the effectiveness of each component

- BRNN vs BLSTM, BLSTM obtains better performance than BRNN on all evaluation metrics of both the two tasks
- BLSTM vs BLSTM-CNN, BLSTM-CNN models significantly outperform the BLSTM model, showing that characterlevel representations are important for linguistic sequence labeling tasks
- BLSTM-CNN-CRF(作者此处笔误，应该是BLSTM-CNN-CRF) models for both POS tagging and NER on all metric.



### 4.3 Comparison with Previous Work

yielding a state-of-the-art performance



#### 4.4 OOV Error Analysis

partition each data set into four subsets

- in-vocabulary words(IV)
- out-of-training-vocabulary words (OOTV)
- out-of-embedding-vocabulary words (OOEV)
- out-of-both-vocabulary words (OOBV)


The largest improvements appear on the OOBV subsets of both the two corpora. This demonstrates that by adding CRF for joint decoding, our model is more powerful on words that are out of both the training and embedding sets.

## 5 Related Work

neural network architectures applied to linguistic sequence labeling such as POS tagging, chunking and NER:

- similar to our model:
  - BLSTM-CRF model proposed by Huang et al. (2015)
  BLSTM for word-level representations and CRF for jointly label decoding, but did not employ CNNs to model character-level information. Second, handcrafted features are used.
  - BLSTM-CNNs model by Chiu and Nichols (2015)
  a hybrid of BLSTM and CNNs to model both character and word-level representations, while did not use CRF for joint decoding and their model utilizes external knowledge such as character-type, capitalization and lexicon features, and some data preprocessing specifically for NER.
  - BLSTM-CRF by Lample et al. (2016)
  utilized BLSTM to model both the character and word-level information, and use data pre-processing the same as Chiu and Nichols (2015). Instead, we use CNN to model character-level information, achieving better NER performance without using any data preprocessing.

- other neural networks:
  - RNN-CNNs model proposed by Labeau et al. (2015) for German POS tagging
  similar to the LSTM-CNNs model in Chiu and Nichols (2015), with the difference of using vanila RNN instead of LSTM
  - CharWNN proposed by Santos and Zadrozny (2014) 
  CNN to model character-level information, which is inspired by the feed-forward network (Collobert et al., 2011). CharWNN obtained near state-of-the-art accuracy on English POS tagging.(see Section 4.3 for details).
  - A similar model has also been applied to Spanish and Portuguese NER (dos Santos et al., 2015) Ling et al. (2015)
  - Yang et al. (2016) also used BSLTM to compose character embeddings to word’s representation, which is similar to Lample et al. (2016).
  - Peng and Dredze (2016) Improved NER for Chinese Social Media with Word Segmentation.

## 6 Conclusion

Conclusion

- It is a truly end-toend model relying on no task-specific resources, feature engineering or data pre-processing
- achieved state-of-the-art performance on two linguistic sequence labeling tasks, comparing with previously state-of-the-art systems

Future Work

- further improved by exploring multi-task learning approaches to combine more useful and correlated information
- apply our model to data from other domains such as social media

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTIwNzMwMTUzMl19
-->