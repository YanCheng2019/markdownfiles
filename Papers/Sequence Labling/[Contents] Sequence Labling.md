**End-to-end Sequence Labeling via Bi-directional LSTM-CNNs-CRF**
Xuezhe MaEduard H. Hovy
Date: 2016/03
Comments: ACL2016
Citations: 707 [2019/8]


**Neural Architectures for Named Entity Recognition**
Guillaume Lample, Miguel Ballesteros, Sandeep Subramanian, Kazuya Kawakami, Chris Dyer

Date: 2016/03
Comments: NAACL2016
Citations: 1089 [2019/8]
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTczNTY2MDUxNV19
-->