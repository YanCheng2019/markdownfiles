# [ACL2016] End-to-end Sequence Labeling via Bi-directional LSTM-CNNs-CRF

<p align="center">
  <img src="https://gitlab.com/YanCheng2019/markdownfiles/raw/master/Papers/Sequence%20Labling/images/1564993714408_2.png">
</p>

**Date**: 2016/03
**Comments**: ACL2016
**Citations**: 707 [2019/8]

## 1 Introduction

**Motivation**

end-to-end sequence labeling, like POS, NER

**Previous works**

**Previous problems**

- hand-crafted features
- even systems that have utilized distributed representations as inputs have used these
to augment, rather than replace, hand-crafted features

**Datasets**

- Penn Treebank WSJ corpus for part-of-speech (POS)
- CoNLL 2003 corpus for named entity recognition (NER)

**Contents**:

- proposing a novel neural network architecture for linguistic sequence labeling. 
- giving empirical evaluations of this model on benchmark data sets for two classic NLP tasks. 
- achieving state-of-the-art performance with this truly end-to-end system.

## 2 Neural Network Architecture

### 2.1 CNN for Character-level Representation

- Previous studies (Santos and Zadrozny, 2014; Chiu and Nichols, 2015) have shown that CNN is an effective approach to extract morphological information (like the prefix or suffix of a word) from characters of words and encode it into neural representations.
  >[Santos, C. D., & Zadrozny, B. (2014). Learning character-level representations for part-of-speech tagging. In _Proceedings of the 31st International Conference on Machine Learning (ICML-14)_ (pp. 1818-1826).](http://www.jmlr.org/proceedings/papers/v32/santos14.pdf)
  >
  >[Chiu, J. P., & Nichols, E. (2016). Named entity recognition with bidirectional LSTM-CNNs. _Transactions of the Association for Computational Linguistics_, _4_, 357-370.](https://www.mitpressjournals.org/doi/pdf/10.1162/tacl_a_00104)
- A dropout layer (Srivastava et al., 2014) is applied before character embeddings are input to CNN.
  >[Srivastava, N., Hinton, G., Krizhevsky, A., Sutskever, I., & Salakhutdinov, R. (2014). Dropout: a simple way to prevent neural networks from overfitting. The journal of machine learning research, 15(1), 1929-1958.](http://www.jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf?utm_content=buffer79b43&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)

Thus, the character-level representation architecture:

<p align="center">
  <img src="https://gitlab.com/YanCheng2019/markdownfiles/raw/master/Papers/Sequence%20Labling/images/1564995378676_3.png">
</p>

### 2.2 BLSTM

- Basically, a LSTM unit is composed of three multiplicative gates which control the proportions of information to forget and to pass on to the next time step.
- For many sequence labeling tasks it is beneficial to have access to both past (left) and future (right) contexts. An elegant solution whose effectiveness has been proven by previous work (Dyer et al., 2015) is bi-directional LSTM (BLSTM). The basic idea is to present each sequence forwards and backwards to two separate hidden states to capture past and future information, respectively. Then the two hidden states are concatenated to form the final output.

### 2.3 CRF

- For sequence labeling (or general structured prediction) tasks, it is beneficial to consider the correlations between labels in neighborhoods and jointly decode the best chain of labels for a given input sentence. Therefore, we model label sequence jointly using a conditional random field (CRF) (Lafferty et al., 2001), instead of decoding each label independently.
  >[Lafferty, J., McCallum, A., & Pereira, F. C. (2001). Conditional random fields: Probabilistic models for segmenting and labeling sequence data.](https://repository.upenn.edu/cgi/viewcontent.cgi?article=1162&context=cis_papers)
>
- For CRF training, we use the maximum conditional likelihood estimation. For a sequence CRF model (only interactions between two successive labels are considered), training and decoding can be solved efficiently by adopting the Viterbi algorithm.

### 2.4 BLSTM-CNNs-CRF

- Step1: for each word, the character-level representation is computed by the CNN with character embeddings as inputs
- Step2: character representation vector is concatenated with the word embedding and feed into the BLSTM network, dropout layers are applied on both the input and output vectors of BLSTM.
- Step3: the output vectors of BLSTM are fed to the CRF layer to jointly decode the best label sequence.

Architecture:

<p align="center">
  <img width="40%" height="40%" src="https://gitlab.com/YanCheng2019/markdownfiles/raw/master/Papers/Sequence%20Labling/images/1564995885572_4.png">
</p>

## 3 Network Training


### 3.1 Parameter Initialization

**Word Embeddings**

4 type word embeddings:

- Stanford's publicly available GloVe 100-dimensional embeddings trained on 6 billion words from Wikipedia and web text (Pennington et al., 2014)
  >[Pennington, J., Socher, R., & Manning, C. (2014, October). Glove: Global vectors for word representation. In Proceedings of the 2014 conference on empirical methods in natural language processing (EMNLP) (pp. 1532-1543).](https://www.aclweb.org/anthology/D14-1162)
- Senna 50-dimensional embeddings trained on Wikipedia and Reuters RCV-1 corpus (Collobert et al., 2011)
  >[Collobert, R., Weston, J., Bottou, L., Karlen, M., Kavukcuoglu, K., & Kuksa, P. (2011). Natural language processing (almost) from scratch. _Journal of machine learning research_, _12_(Aug), 2493-2537.](http://www.jmlr.org/papers/volume12/collobert11a/collobert11a.pdf)
- Google's Word2Vec 300-dimensional embeddings3 trained on 100 billion words from Google News (Mikolov et al., 2013)
  >[Mikolov, T., Sutskever, I., Chen, K., Corrado, G. S., & Dean, J. (2013). Distributed representations of words and phrases and their compositionality. In _Advances in neural information processing systems_ (pp. 3111-3119).](http://papers.nips.cc/paper/5021-distributed-representations-of-words-and-phrases-and-their-compositionality.pdf)
- randomly initialized embeddings with 100 dimensions, where embeddings are uniformly sampled from range $\left[ -\sqrt{\frac{3}{dim}}, + \sqrt{\frac{3}{dim}} \right]$ where dim is the dimension of embeddings (He et al., 2015)
  >[He, K., Zhang, X., Ren, S., & Sun, J. (2015). Delving deep into rectifiers: Surpassing human-level performance on imagenet classification. In _Proceedings of the IEEE international conference on computer vision_ (pp. 1026-1034).](https://www.cv-foundation.org/openaccess/content_iccv_2015/papers/He_Delving_Deep_into_ICCV_2015_paper.pdf)

experiments result:

- models using pretrained word embeddings obtain a significant improvement as opposed to the ones using random embeddings
- NER relies more heavily on pretrained embeddings than POS tagging. This is consistent with results reported by previous work (Collobert et al., 2011; Huang et al., 2015; Chiu and Nichols, 2015).
  > [Huang, Z., Xu, W., & Yu, K. (2015). Bidirectional LSTM-CRF models for sequence tagging. _arXiv preprint arXiv:1508.01991_.](https://arxiv.org/pdf/1508.01991.pdf)
  >
  >[Chiu, J. P., & Nichols, E. (2016). Named entity recognition with bidirectional LSTM-CNNs. _Transactions of the Association for Computational Linguistics_, _4_, 357-370.](https://www.aclweb.org/anthology/Q16-1026)
- Google's Word2Vec is not as good as the other two embeddings on NER is because of vocabulary mismatch. Word2Vec embeddings were trained in casesensitive manner, excluding many common symbols such as punctuations and digits.

**Character Embeddings**

- Character embeddings are initialized with uniform samples from $\left[ -\sqrt{\frac{3}{dim}}, + \sqrt{\frac{3}{dim}} \right]$ where dim is 30.

**Weight Matrices and Bias Vectors**

- Matrix parameters are randomly initialized with uniform samples from $\left[ -\sqrt{\frac{6}{r+c}}, + \sqrt{\frac{6}{r+c}} \right]$, where r and c are the number of of rows and columns in the structure (Glorot and Bengio, 2010).
- Bias vectors are initialized to zero, except the bias bf for the forget gate in LSTM , which is initialized to 1.0 (Jozefowicz et al., 2015).
  > [Glorot, X., & Bengio, Y. (2010, March). Understanding the difficulty of training deep feedforward neural networks. In _Proceedings of the thirteenth international conference on artificial intelligence and statistics_ (pp. 249-256).](http://proceedings.mlr.press/v9/glorot10a/glorot10a.pdf)
  >
  >[Jozefowicz, R., Zaremba, W., & Sutskever, I. (2015, June). An empirical exploration of recurrent network architectures. In _International Conference on Machine Learning_ (pp. 2342-2350).](http://proceedings.mlr.press/v37/jozefowicz15.pdf)


### 3.2 Optimization Algorithm

**Algorithm**

- SGD, Best in experiments.
  batch size: 10, momentum: 0.9, initial learning rate: $η_0 = 0.01$ for POS tagging, and $η_0 = 0.015$ for NER. The learning rate is updated on each epoch of training as $η_t = η_0/(1 + ρ_t)$, with decay rate $ρ = 0.05$ and $t$ is the number of epoch completed. gradient clipping is 5.0, to reduce the effects of “gradient exploding” (Pascanu et al., 2012).
  >[Pascanu, R., Mikolov, T., & Bengio, Y. (2013, February). On the difficulty of training recurrent neural networks. In _International conference on machine learning_ (pp. 1310-1318).](http://proceedings.mlr.press/v28/pascanu13.pdf)
- AdaDelta: (Zeiler, 2012)
  >[Zeiler, M. D. (2012). ADADELTA: an adaptive learning rate method. _arXiv preprint arXiv:1212.5701_.](https://arxiv.org/pdf/1212.5701.pdf)
- Adam: (Kingma and Ba, 2014)
  >[Kingma, D. P., & Ba, J. (2014). Adam: A method for stochastic optimization. _arXiv preprint arXiv:1412.6980_.](https://arxiv.org/pdf/1412.6980.pdf)
- RMSProp: (Dauphin et al., 2015)
  >[Dauphin, Y., De Vries, H., & Bengio, Y. (2015). Equilibrated adaptive learning rates for non-convex optimization. In _Advances in neural information processing systems_ (pp. 1504-1512).](http://papers.nips.cc/paper/5870-equilibrated-adaptive-learning-rates-for-non-convex-optimization.pdf)

**Early Stopping**

We use early stopping (Giles, 2001; Graves et al., 2013) based on performance on validation sets.

>[Caruana, R., Lawrence, S., & Giles, C. L. (2001). Overfitting in neural nets: Backpropagation, conjugate gradient, and early stopping. In _Advances in neural information processing systems_ (pp. 402-408).](http://papers.nips.cc/paper/1895-overfitting-in-neural-nets-backpropagation-conjugate-gradient-and-early-stopping.pdf)
>
>[Graves, A., Mohamed, A. R., & Hinton, G. (2013, May). Speech recognition with deep recurrent neural networks. In _2013 IEEE international conference on acoustics, speech and signal processing_ (pp. 6645-6649). IEEE.](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6638947)


**Fine Tuning**

For each of the embeddings, we fine-tune initial embeddings, modifying them during gradient updates of the neural network model by back-propagating gradients. The effectiveness of this method has been previously explored in sequential and structured prediction problems (Collobert et al., 2011; Peng and Dredze, 2015).
>[Peng, N., & Dredze, M. (2015, September). Named entity recognition for chinese social media with jointly trained embeddings. In _Proceedings of the 2015 Conference on Empirical Methods in Natural Language Processing_ (pp. 548-554).](https://www.aclweb.org/anthology/D15-1064)

**Dropout Training**

To mitigate overfitting, we apply the dropout method (Srivastava et al., 2014) to regularize our model. We fix dropout rate at 0.5 for all dropout layers through all the experiments. We obtain significant improvements on model performance after using dropout.

### 3.3 Tuning Hyper-Parameters

Tune the hyper-parameters on the development sets by random search. Due to time constrains it is infeasible to do a random search across the full hyper-parameter space. Thus, for the tasks of POS tagging and NER we try to share as many hyper-parameters as possible. Note that the final hyper-parameters for these two tasks are almost the same, except the initial learning rate. 

<p align="center">
  <img src="https://gitlab.com/YanCheng2019/markdownfiles/raw/master/Papers/Sequence%20Labling/images/1565005397041_5.png">
</p>

- We set the state size of LSTM to 200. Tuning this parameter did not significantly impact
the performance of our model.
- For CNN, we use 30 filters with window length 3.

## 4 Experiments

### 4.1 Data Sets

- POS Tagging
Wall Street Journal (WSJ) portion of Penn Treebank (PTB) (Marcus et al., 1993), 45 different POS tags, standard splits(section 0-18 as training, section 19-21 as development, section 22-24 as test) (Manning, 2011; Søgaard, 2011).
  >[Marcus, M., Santorini, B., & Marcinkiewicz, M. A. (1993). Building a large annotated corpus of English: The Penn Treebank.](https://repository.upenn.edu/cgi/viewcontent.cgi?article=1246&context=cis_reports)

- NER
English data from CoNLL 2003 shared task (Tjong Kim Sang and De Meulder, 2003), 4 different named entities (PERSON, LOCATION, ORGANIZATION, and MISC). We use the BIOES tagging scheme instead of standard BIO2, as previous studies have reported meaningful improvement with this scheme (Ratinov and Roth, 2009; Dai et al., 2015; Lample et al., 2016).
  >[Sang, E. F., & De Meulder, F. (2003). Introduction to the CoNLL-2003 shared task: Language-independent named entity recognition. _arXiv preprint cs/0306050_.](https://arxiv.org/pdf/cs/0306050.pdf)
  >
  >[Ratinov, L., & Roth, D. (2009, June). Design challenges and misconceptions in named entity recognition. In _Proceedings of the thirteenth conference on computational natural language learning_ (pp. 147-155). Association for Computational Linguistics.](http://delivery.acm.org/10.1145/1600000/1596399/p147-ratinov.pdf)
  >
  > [Lample, G., Ballesteros, M., Subramanian, S., Kawakami, K., & Dyer, C. (2016). Neural architectures for named entity recognition. _arXiv preprint arXiv:1603.01360_.](https://arxiv.org/pdf/1603.01360.pdf)

### 4.2 dissect the effectiveness of each component

- BRNN vs BLSTM, BLSTM obtains better performance than BRNN on all evaluation metrics of both the two tasks
- BLSTM vs BLSTM-CNN, BLSTM-CNN models significantly outperform the BLSTM model, showing that characterlevel representations are important for linguistic sequence labeling tasks
- BLSTM-CNN-CRF(作者此处笔误，应该是BLSTM-CNN-CRF) models for both POS tagging and NER on all metric.

<p align="center">
  <img src="https://gitlab.com/YanCheng2019/markdownfiles/raw/master/Papers/Sequence%20Labling/images/1565006626599_6.png">
</p>

### 4.3 Comparison with Previous Work

yielding a state-of-the-art performance

<p align="center">
  <img src="https://gitlab.com/YanCheng2019/markdownfiles/raw/master/Papers/Sequence%20Labling/images/1565006854855_7.png">
</p>

#### 4.4 OOV Error Analysis

partition each data set into four subsets

- in-vocabulary words(IV)
- out-of-training-vocabulary words (OOTV)
- out-of-embedding-vocabulary words (OOEV)
- out-of-both-vocabulary words (OOBV)

<p align="center">
  <img src="https://gitlab.com/YanCheng2019/markdownfiles/raw/master/Papers/Sequence%20Labling/images/1565007507839_8.png">
</p>

The largest improvements appear on the OOBV subsets of both the two corpora. This demonstrates that by adding CRF for joint decoding, our model is more powerful on words that are out of both the training and embedding sets.

## 5 Related Work

neural network architectures applied to linguistic sequence labeling such as POS tagging, chunking and NER:

- similar to our model:
  - BLSTM-CRF model proposed by Huang et al. (2015)
  BLSTM for word-level representations and CRF for jointly label decoding, but did not employ CNNs to model character-level information. Second, handcrafted features are used.
  - BLSTM-CNNs model by Chiu and Nichols (2015)
  a hybrid of BLSTM and CNNs to model both character and word-level representations, while did not use CRF for joint decoding and their model utilizes external knowledge such as character-type, capitalization and lexicon features, and some data preprocessing specifically for NER.
  - BLSTM-CRF by Lample et al. (2016)
  utilized BLSTM to model both the character and word-level information, and use data pre-processing the same as Chiu and Nichols (2015). Instead, we use CNN to model character-level information, achieving better NER performance without using any data preprocessing.

- other neural networks:
  - RNN-CNNs model proposed by Labeau et al. (2015) for German POS tagging
  similar to the LSTM-CNNs model in Chiu and Nichols (2015), with the difference of using vanila RNN instead of LSTM
  - CharWNN proposed by Santos and Zadrozny (2014) 
  CNN to model character-level information, which is inspired by the feed-forward network (Collobert et al., 2011). CharWNN obtained near state-of-the-art accuracy on English POS tagging.(see Section 4.3 for details).
  - A similar model has also been applied to Spanish and Portuguese NER (dos Santos et al., 2015) Ling et al. (2015)
  - Yang et al. (2016) also used BSLTM to compose character embeddings to word’s representation, which is similar to Lample et al. (2016).
  - Peng and Dredze (2016) Improved NER for Chinese Social Media with Word Segmentation.

## 6 Conclusion

Conclusion

- It is a truly end-toend model relying on no task-specific resources, feature engineering or data pre-processing
- achieved state-of-the-art performance on two linguistic sequence labeling tasks, comparing with previously state-of-the-art systems

Future Work

- further improved by exploring multi-task learning approaches to combine more useful and correlated information
- apply our model to data from other domains such as social media

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTMzOTcyMzA5NywxOTA4OTk2MTUzLDEyOD
Y0NzYzMDEsLTUxOTQ5OTA4MCwtMjA1NDY4NzIwMiwxOTg5Mjk5
NjE5LDE4ODMwMDExNTUsLTk3MTc3MTc1MiwtNzIyODgxNDQ4LC
0xODc1ODA0NTksMjAxODI4NTAsLTkyMjI5NDA0MiwtOTIyMjk0
MDQyLC0xODIyMzM1NzkyLC01NzU3NjgwNTVdfQ==
-->